const path = require('path')

const components = require('./components')
const bookBuilder = require('./modules/book-builder')
const winston = require('winston')
// const authsomeMode = require('./modules/authsome')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
    }),
  ],
})

module.exports = {
  authsome: {
    mode: require.resolve('./modules/authsome'),
    teams: {
      productionEditor: {
        name: 'Production Editor',
      },
      copyEditor: {
        name: 'Copy Editor',
      },
      author: {
        name: 'Author',
      },
    },
  },
  bookBuilder,
  epub: {
    fontsPath: '/uploads/fonts',
  },
  'password-reset': {
    url: 'http://localhost:3000/password-reset',
    sender: 'dev@example.com',
  },
  mailer: {
    from: 'dev@example.com',
    transport: {
      host: 'smtp.mailgun.org',
      auth: {
        user: 'dev@example.com',
        pass: 'password',
      },
    },
  },
  publicKeys: [
    'authsome',
    'bookBuilder',
    'pubsweet',
    'pubsweet-client',
    'pubsweet-server',
    'validations',
  ],
  pubsweet: {
    components,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    'login-redirect': '/',
    navigation: 'app/components/Navigation/Navigation.jsx',
    routes: 'app/routes.jsx',
    theme: 'ThemeEditoria',
    converter: 'ucp',
  },
  'pubsweet-server': {
    db: {},
    sse: true,
    logger,
    port: 3000,
    uploads: 'uploads',
  },
  '@pubsweet/component-polling-server': {
    timer: 10000,
  },
  validations: path.join(__dirname, 'modules', 'validations'),
}
